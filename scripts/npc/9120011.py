# SDC Shop Quick Move

MAPLE_ADMINISTRATOR = 2007

mesoCost = [10000000, 10000000, 100000000]

onyxItems = [
[1003863, 1052612, 1132228, 1102562, 1122252, 1012376, 1113034], #OnyxEquips
[1212066, 1222061, 1232060, 1242065, 1302277, 1312155, 1322205, 1332227, 1362092, 1342092, 1372179, 1382211, 1402199, 1412137, 1422142, 1432169, 1442225, 1452207, 1462195, 1472216, 1482170, 1492181, 1522096, 1532100, 1252064, 1542070, 1552070, 2048910], #OnyxWeapons
[1352605, 1352008, 1352108, 1352405, 1352505, 1352705, 1352205, 1352215, 1352225, 1352235, 1352245, 1352255, 1352265, 1352275, 1352285, 1352295, 1352905, 1352915, 1352927, 1352934, 1352944, 1352955, 1352965, 1352974, 1353005, 1353104, 1352814, 1352806, 1352825] #Maple Black Secondaries

]

leafID = 4001126


leafAmount = [1000, 1000, 5000]

sm.setSpeakerID(MAPLE_ADMINISTRATOR)

selection1 = sm.sendNext("Hello #r#h0##k! How can I help you today?"
            "#b\r\n#L0#Onyx Maple Equips#l"
            "\r\n#L1#Onyx Maple Weapons#l"
            "\r\n#L2#Maple Black Secondaries"
            
            )
#Onyx Maple Equips
listStr = "What item would you like to make? #b"
i = 0
while i < len(onyxItems[selection1]):
    listStr += "\r\n#L" + str(i) + "##i" + str(onyxItems[selection1][i]) + "##z" + str(onyxItems[selection1][i]) + "#"
    i += 1
selection2 = sm.sendNext(listStr)

materialStr = "You want an #i " + str(onyxItems[selection1][selection2]) +  "##z" + str(onyxItems[selection1][selection2]) + "#?" + "\r\nI'm going to need specific items from you in order to make it."

if mesoCost and leafAmount > 0:
    materialStr += "\r\n#i4031138#" + str(mesoCost[selection1]) + " Mesos" + "\r\n#i4001126#" + str(leafAmount[selection1]) + " Maple Leaves"

response = sm.sendAskYesNo(materialStr)

if response:

    if sm.getMesos() < mesoCost[selection1]:
			sm.sendSayOkay("#bYou need atleast " + "#r" + str(mesoCost[selection1]) + " #bMesos!")
			sm.dispose()

    elif sm.hasItem(leafID, leafAmount[selection1]) == False:
			sm.sendSayOkay("#bYou need atleast " + "#r" + str(leafAmount[selection1]) + " #bMaple Leaves")
			sm.dispose()

    else:
        if not sm.canHold(onyxItems[selection1][selection2]):
            sm.sendSayOkay("You do not have enough room in your inventory.")

        else:
            sm.giveMesos(-(mesoCost[selection1]))
            sm.consumeItem(leafID, leafAmount[selection1])
            sm.giveItem(onyxItems[selection1][selection2])
            sm.sendSayOkay("#bThanks! Come back if you need anything else.")

else:
    sm.sendSayOkay("Let me know when you are ready to create something.")
    sm.dispose()











