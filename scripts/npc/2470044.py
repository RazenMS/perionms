# Donation Shop

AGENT_M = 2470044

donationPointCost = [
    [5000, 1000, 1000, 1000, 1000, 1000], # Equipment
    [500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500], # Damage Skins
    [], # Pet Vac
    [150, 150, 150, 150, 150, 100, 150, 150, 1000], # Passive Skills
    [1000, 1500, 2500, 4000, 3000, 5000], # Extra Pend
    [], # Rate Boosters
    [1000, 1000, 1000] # Donor Skills
]

donorItems = [
    [1672069, 1662002, 1662003, 1122210, 1132183, 1152101], # Equipment
    [5680395, 5680343, 2433571, 2434530, 2432532, 2435179, 2434529, 2432592, 2433913, 2432749, 2432154, 2432526], # Damage Skins
    [4030003], # Pet Vacs
    [], # Passive Skills
    [], # Extra Pendant Slot
    [], # Rate Boosters
    [] # Donor Skills
]

# Functions

# Checks for different catches when buying a passive skill.
def canPurchasePassive(skillId, cost):
    if(sm.hasSkill(skillId)):
        sm.sendSayOkay("You've already purchased this passive!")
        sm.dispose()
    if(sm.getDonationPoints() < cost):
        sm.sendSayOkay("Sorry you don't have enough donation points to purchase this!")
        sm.dispose()

# End Functions

sm.setSpeakerID(AGENT_M)

selection1 = sm.sendNext("Hello #r#h0##k! Welcome to the Donation shop! You have " + str(sm.getDonationPoints()) + " donation points!"
                                                                                                                   "#b\r\n#L0#Equipment#l"
                                                                                                                   "\r\n#L1#Damage Skins#l"
                                                                                                                   "\r\n#L2#Pet Vacs"
                                                                                                                   "\r\n#L3#Passive Skills"
                                                                                                                   "\r\n#L4#Extra Pendent Slots"
                                                                                                                   "\r\n#L5#Rate Boosters"
                                                                                                                   "\r\n#L6#Donor Skills"

                         )
#Equipment and Damage skin
if(selection1 == 0 or selection1 == 1):

    listStr = "What would you like to purchase? #b"
    i = 0
    while i < len(donorItems[selection1]):
        listStr += "\r\n#L" + str(i) + "##i" + str(donorItems[selection1][i]) + "##z" + str(donorItems[selection1][i]) + "#"
        i += 1


    selection2 = sm.sendNext(listStr)

    materialStr = "You want an #i " + str(donorItems[selection1][selection2]) +  "##z" + str(donorItems[selection1][selection2]) + "#?" + "\r\nI'm going to need some donation points to generate it!"

    if donationPointCost > 0:
        materialStr += "\r\n#i3993001#" + str(donationPointCost[selection1][selection2]) + " DP"

    response = sm.sendAskYesNo(materialStr)

    if(response):
        if(sm.getDonationPoints() < donationPointCost[selection1][selection2]):
            sm.sendSayOkay("Sorry you don't have enough donation points to purchase this item!")
            sm.dispose()
        sm.setDonationPoints(sm.getDonationPoints() - donationPointCost[selection1][selection2])
        sm.giveItem(donorItems[selection1][selection2])
        sm.sendSayOkay("You've purchased a #i" + str(donorItems[selection1][selection2]) + "##z" + str(donorItems[selection1][selection2]) + " for " + str(donationPointCost[selection1][selection2]) + "points..")
    else:
        sm.sendSayOkay("That's too bad, feel free to come back and purchase something!")
        sm.dispose()


elif (selection1 == 2):
    vacString = """How many hours would you like to purchase? \n
    10 donation points/hour, max 720 hours..\n
    Bonus time for buying longer vacs.\n
    -------------------------------------------\n
    24 hours 230 DP(1-Day)\n
    72 hours 670 DP(3-Days)\n
    168 hours 1500 DP(7-Days)\n
    720 hours 5000 DP(30 Days)"""

    numOfHours = sm.sendAskNumber(vacString, 1, 1, 720)

    if (numOfHours == 24):
        donorPointCost = 230
    elif (numOfHours == 72):
        donorPointCost = 670
    elif(numOfHours == 168):
        donorPointCost = 1500
    elif(numOfHours == 720):
        donorPointCost = 5000
    else:
        donorPointCost = numOfHours * 10

    finalAmountStr = "Would you like to purchase " + str(numOfHours) + "-hour of pet vac for: \n" + "\r\n#i3993001#" + str(donorPointCost) + " DP"
    response = sm.sendAskYesNo(finalAmountStr)

    if (response):
        if (sm.getDonationPoints() < donorPointCost):
            sm.sendSayOkay("#bYou need at least " + "#r" + str(donorPointCost) + " #bDonation Points!")
            sm.dispose()
        else:
            sm.setDonationPoints(sm.getDonationPoints() - donorPointCost)
            sm.giveItem(4030003, 1, numOfHours)
            sm.sendSayOkay("#bThanks! Come back if you need anything else.")
            sm.dispose()

elif (selection1 == 3):
    passiveSelection = sm.sendNext("What donor passive would you like to buy?"
                                   "#b\r\n#L0#+ 80 All-Stats 30 Att/Matt - 150 DP"
                                   "\r\n#L1#+ 10% All-Stats - 150 DP"
                                   "\r\n#L2#+ 10% Boss 10% IED - 150 DP"
                                   "\r\n#L3#+ 13% Min Crit 9% Max Crit - 150 DP"
                                   "\r\n#L4#+ Recover 5% of Damage to Max HP when attacking - 150 DP"
                                   "\r\n#L5#+ 400 Weapon/Magic Defense - 100 DP"
                                   "\r\n#L6#+ 10% Meso/Drop - 150 DP"
                                   "\r\n#L7#+ 10% Summon Duration - 150 DP"
                                   "\r\n#L8#+ 20% Buff Duration - 150 DP"
                                   "\r\n#L9#Purchase All Donor Passives - 1000 DP"
                                   )
    if(passiveSelection == 0): # Flat All-Stat, Att/Matt
        # Checks if already purchase and user has valid amount of donation points
        canPurchasePassive(71000271, donationPointCost[selection1][passiveSelection])

        sm.giveSkill(71000271, 4) # 80 Int
        sm.giveSkill(71000361, 4) # 20 All-Stat
        sm.giveSkill(71000611, 3) # 80 Str
        sm.giveSkill(71000651, 3) # 80 Dex
        sm.giveSkill(70000003, 40) # 80 Luk

        # Att and matt
        sm.giveSkill(70000012, 30)
        sm.giveSkill(70000013, 30)

        sm.setDonationPoints(sm.getDonationPoints() - donationPointCost[selection1][passiveSelection])
        sm.sendSayOkay("Thanks for your purchase!\r\n" + str(donationPointCost[selection1][passiveSelection]) + " donation points have been deducted from your account.")
        sm.warp(910000000)
        sm.dispose()

    elif(passiveSelection == 1): # %All-Stat
        canPurchasePassive(80000047, donationPointCost[selection1][passiveSelection])

        sm.giveSkill(80000047, 2) # % All Stat

        sm.setDonationPoints(sm.getDonationPoints() - donationPointCost[selection1][passiveSelection])
        sm.sendSayOkay("Thank you for your purchase!\r\nThe % All Stat has been added to your character!\r\n" + str(donationPointCost[selection1][passiveSelection]) + " donation points have been deducted from your account.")
        sm.warp(910000000)
        sm.dispose()
    elif(passiveSelection == 2): # %Boss/IED
        canPurchasePassive(80000001, donationPointCost[selection1][passiveSelection])

        sm.giveSkill(80000001, 1) # DS Link Skill Lv 3
        sm.giveSkill(80000005, 1) # Lumi Link Skill Lv 3

        sm.setDonationPoints(sm.getDonationPoints() - donationPointCost[selection1][passiveSelection])
        sm.sendSayOkay("Thank you for your purchase!\r\nThe % Boss and % IED has been added to your character!\r\n" + str(donationPointCost[selection1][passiveSelection]) + " donation points have been deducted from your account.")
        sm.warp(910000000)
        sm.dispose()
    elif(passiveSelection == 3): # %Min/Max Crit
        canPurchasePassive(71001003, donationPointCost[selection1][passiveSelection])

        sm.giveSkill(71001003, 4) # Max Crit
        sm.giveSkill(71000251, 4) # Min Crit
        sm.giveSkill(71000411, 4)

        sm.setDonationPoints(sm.getDonationPoints() - donationPointCost[selection1][passiveSelection])
        sm.sendSayOkay("Thank you for your purchase!\r\nThe % Min/Max has been added to your character!\r\n" + str(donationPointCost[selection1][passiveSelection]) + " donation points have been deducted from your account.")
        sm.warp(910000000)
        sm.dispose()
    elif(passiveSelection == 4): # Recover hp off attack
        canPurchasePassive(31010002, donationPointCost[selection1][passiveSelection])

        sm.giveSkill(31010002, 10) # Life Sap

        sm.setDonationPoints(sm.getDonationPoints() - donationPointCost[selection1][passiveSelection])
        sm.sendSayOkay("Thank you for your purchase!\r\nThe % HP restore on attacks has been added to your character!\r\n" + str(donationPointCost[selection1][passiveSelection]) + " donation points have been deducted from your account.")
        sm.warp(910000000)
        sm.dispose()
    elif(passiveSelection == 5): # Weapon/Magic Defense
        canPurchasePassive(70000006, donationPointCost[selection1][passiveSelection])

        sm.giveSkill(70000006, 40) # Wep Def
        sm.giveSkill(70000007, 40) # MDef

        sm.setDonationPoints(sm.getDonationPoints() - donationPointCost[selection1][passiveSelection])
        sm.sendSayOkay("Thank you for your purchase!\r\nThe Weapon and Magic defense has been added to your character!\r\n" + str(donationPointCost[selection1][passiveSelection]) + " donation points have been deducted from your account.")
        sm.warp(910000000)
        sm.dispose()
    elif(passiveSelection == 6): # Meso/Drop
        canPurchasePassive(70000049, donationPointCost[selection1][passiveSelection])

        sm.giveSkill(70000049, 20) # Drop
        sm.giveSkill(70000050, 20) # Meso

        sm.setDonationPoints(sm.getDonationPoints() - donationPointCost[selection1][passiveSelection])
        sm.sendSayOkay("Thank you for your purchase!\r\nThe % Meso and Drop has been added to your character!\r\n" + str(donationPointCost[selection1][passiveSelection]) + " donation points have been deducted from your account.")
        sm.warp(910000000)
        sm.dispose()
    elif(passiveSelection == 7): # Summon Duration
        canPurchasePassive(71000052, donationPointCost[selection1][passiveSelection])

        sm.giveSkill(71000052, 4)

        sm.setDonationPoints(sm.getDonationPoints() - donationPointCost[selection1][passiveSelection])
        sm.sendSayOkay("Thank you for your purchase!\r\nThe % Summon Duration has been added to your character!\r\n" + str(donationPointCost[selection1][passiveSelection]) + " donation points have been deducted from your account.")
        sm.warp(910000000)
        sm.dispose()
    elif(passiveSelection == 8): # Buff Duration
        canPurchasePassive(71000351, donationPointCost[selection1][passiveSelection - 1])

        sm.giveSkill(71000351, 4)

        sm.setDonationPoints(sm.getDonationPoints() - donationPointCost[selection1][passiveSelection - 1])
        sm.sendSayOkay("Thank you for your purchase!\r\nThe % Buff Duration has been added to your character!\r\n" + str(donationPointCost[selection1][passiveSelection]) + " donation points have been deducted from your account.")
        sm.warp(910000000)
        sm.dispose()
    elif(passiveSelection == 9): # All passives
        if(sm.hasSkill(71000271) and sm.hasSkill(80000047) and sm.hasSkill(80000001) and sm.hasSkill(71001003) and
                sm.hasSkill(31010002) and sm.hasSkill(70000006) and sm.hasSkill(70000049) and sm.hasSkill(71000052) and
                sm.hasSkill(71000351)):
            sm.sendSayOkay("Sorry this character already has all the passives..")
            sm.dispose()
        if(sm.getDonationPoints() < donationPointCost[selection1][passiveSelection - 1]):
            sm.sendSayOkay("Sorry you don't have enough donation points to purchase this!")
            sm.dispose()
        sm.giveSkill(71000271, 4) # 80 Int
        sm.giveSkill(71000361, 4) # 20 All-Stat
        sm.giveSkill(71000611, 3) # 80 Str
        sm.giveSkill(71000651, 3) # 80 Dex
        sm.giveSkill(70000003, 40) # 80 Luk
        sm.giveSkill(70000012, 30) # 30 Att
        sm.giveSkill(70000013, 30) # 30 MAtt
        sm.giveSkill(80000047, 2) # % All Stat
        sm.giveSkill(80000001, 1) # DS Link Skill Lv 3
        sm.giveSkill(80000005, 1) # Lumi Link Skill Lv 3
        sm.giveSkill(71001003, 4) # Max Crit
        sm.giveSkill(71000251, 4) # Min Crit
        sm.giveSkill(71000411, 4) # Min Crit 2
        sm.giveSkill(31010002, 10) # Life Sap
        sm.giveSkill(70000006, 40) # Wep Def
        sm.giveSkill(70000007, 40) # MDef
        sm.giveSkill(70000049, 20) # Drop
        sm.giveSkill(70000050, 20) # Meso
        sm.giveSkill(71000052, 4) # Summon Duration
        sm.giveSkill(71000351, 4) # Buff Duration

        sm.setDonationPoints(sm.getDonationPoints() - donationPointCost[selection1][passiveSelection - 1])
        sm.sendSayOkay("Thank you for your purchase!\r\nAll donor passives have been added to your character!\r\n" + str(donationPointCost[selection1][passiveSelection - 1]) + " points have been deducted from your account!")
        sm.warp(910000000)
        sm.dispose()
elif(selection1 == 4):
    sm.sendSayOkay("/TODO")
    sm.dispose()
elif(selection1 == 5):
    sm.sendSayOkay("/TODO")
    sm.dispose()
elif(selection1 == 6):
    sm.sendSayOkay("/TODO")
    sm.dispose()
else:
    sm.sendSayOkay("Something went wrong, please talk to the npc again!")
    sm.dispose()


sm.sendSayOkay("Let me know when you are ready to buy something!")
sm.dispose()