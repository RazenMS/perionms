#Free Market Quick Move Warp

#Free Market Field ID
MAPLE_ADMIN = 2007

sm.setSpeakerID(MAPLE_ADMIN)
map = 910000000

if sm.getFieldID() == 910000000:
	sm.sendSayOkay("You are already in the #bFree Market.")
	sm.dispose()

response = sm.sendAskYesNo("Would you like to go to the #bFree Market?")

if response:
    sm.warp(map, 0)
